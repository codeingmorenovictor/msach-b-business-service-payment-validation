/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.api;

import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.service.HolidaysService;
import com.bancoppel.commons.annotation.ValidateHeaders;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Clase controller con endpoints de conexión a servicio de dias feriados y validación de los mismos
 * 
 * @author Victor Moreno, Nova.
 */
@RestController
@Validated
@RequestMapping(ApiConstants.BASE_PATH)
public class HolidaysController {


  /**
   * Servicio holidaysService para consulta de dias feriados y/o no laborables
   */
  @Autowired
  private HolidaysService holidaysService;


  /**
   * Metodo que valida si la fecha actual es o no un dia feriado y devuelve estatus.
   *
   * @param accept Header que representa el accept.
   * @param authorization Header que representa el authorization.
   * @param contentType Header que representa el contentType.
   * @param channelId Header que representa el channelId.
   * @param UUID Header que representa el uuid.
   * @return Retorna un body vacío solo con status.
   */
  @ApiOperation(value = ApiConstants.OPERATION_API, consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {@ApiResponse(code = ApiConstants.CODE_OK, message = ApiConstants.OK),
      @ApiResponse(code = ApiConstants.CODE_BAD_REQUEST, message = ApiConstants.BAD_REQUEST),
      @ApiResponse(code = ApiConstants.CODE_INTERNAL_ERROR, message = ApiConstants.INTERNAL_ERROR)})
  @ValidateHeaders
  @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, path = ApiConstants.HOLIDAY_ENDPOINT)
  public ResponseEntity<Void> getHolidayStatus(

      @RequestHeader(name = HttpHeaders.ACCEPT) String accept,
      @RequestHeader(name = HttpHeaders.AUTHORIZATION) String authorization,
      @RequestHeader(name = ApiConstants.REFRESH_TOKEN) String refreshToken,
      @RequestHeader(name = HttpHeaders.CONTENT_TYPE,
          defaultValue = StringUtils.EMPTY) String contentType,
      @RequestHeader(name = ApiConstants.CHANNEL_ID,
          defaultValue = StringUtils.EMPTY) String channelId,
      @RequestHeader(name = ApiConstants.UUID) String uuid,
      @RequestHeader(name = ApiConstants.DEVICE_ID) String deviceId) {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.ACCEPT, accept);
    headers.add(HttpHeaders.AUTHORIZATION, authorization);
    headers.add(ApiConstants.REFRESH_TOKEN, refreshToken);
    headers.add(HttpHeaders.CONTENT_TYPE, contentType);
    headers.add(ApiConstants.CHANNEL_ID, channelId);
    headers.add(ApiConstants.UUID, uuid);
    headers.add(ApiConstants.DEVICE_ID, deviceId);
    holidaysService.getResponseStatusAccordingToCurrentDate(headers);
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
