/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.business;

import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.constant.HolidaysConstants;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.DownstreamException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.HolidayForbiddenAccessException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.HolidayNoDataAccessException;
import com.bancoppel.channel.service.payment.validation.feign.HolidaysFeignClient;
import com.bancoppel.channel.service.payment.validation.model.holiday.HolidayModel;
import com.bancoppel.channel.service.payment.validation.model.holiday.HolidayResponse;
import com.bancoppel.channel.service.payment.validation.service.HolidaysService;
import com.bancoppel.commons.annotation.HandledProcedure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Clase implementación de lógica de interface HolidaysService.
 * 
 * @author Victor Moreno
 */
@Service
public class HolidayServiceImpl implements HolidaysService {

  /**
   * Feign client para consulta de dias feriados y/o no laborables
   */
  @Autowired
  private HolidaysFeignClient holidayClient;

  /**
   * Metodo que valida la fecha actual contra listado de dias feriados del banco y devuelve un
   * HttpStatus
   */
  @HandledProcedure(name = ApiConstants.LOG_HOLIDAY_SERVICE,
      value = ApiConstants.LOG_HOLIDAY_SERVICE,
      ignoreExceptions = {HolidayForbiddenAccessException.class, HolidayNoDataAccessException.class,
          DownstreamException.class})
  @Override
  public void getResponseStatusAccordingToCurrentDate(HttpHeaders headers) {
    HolidayResponse holly =
        holidayClient.getHolidays(headers).orElseThrow(HolidayNoDataAccessException::new);

    String currentDate = holly.getToday();

    List<HolidayModel> filteredListByNonWorkableDays = holly.getHolidays().stream()
        .filter(h -> h.getHoliday()
            .substring(HolidaysConstants.HOLIDAY_SUBSTRING_START,
                HolidaysConstants.HOLIDAY_SUBSTRING_END)
            .equals(HolidaysConstants.JANUARY_FIRST)
            || h.getHoliday()
                .substring(HolidaysConstants.HOLIDAY_SUBSTRING_START,
                    HolidaysConstants.HOLIDAY_SUBSTRING_END)
                .equals(HolidaysConstants.DECEMBER_TWENTYFIVE))
        .collect(Collectors.toList());

    Optional<HolidayModel> holidayMatched = filteredListByNonWorkableDays.stream().findFirst()
        .filter(h -> h.getHoliday().equals(currentDate));

    if (holidayMatched.isPresent() || isCurrentHourOutOfRange()) {
      throw new HolidayForbiddenAccessException();
    }

  }

  /**
   * Metodo que valida si la hora actual esta o no dentro del horario de operaciones del banco.
   * 
   */
  public boolean isCurrentHourOutOfRange() {
    LocalTime currentHour = LocalTime.now();
    LocalTime minHourAllowedForTransactions = LocalTime.of(HolidaysConstants.MIN_HOUR,
        Integer.valueOf(HolidaysConstants.ZERO_VAL_FOR_MIN_OR_SECS),
        Integer.valueOf(HolidaysConstants.ZERO_VAL_FOR_MIN_OR_SECS));
    LocalTime maxHourAllowedForTransactions = LocalTime.of(HolidaysConstants.MAX_HOUR,
        Integer.valueOf(HolidaysConstants.ZERO_VAL_FOR_MIN_OR_SECS),
        Integer.valueOf(HolidaysConstants.ZERO_VAL_FOR_MIN_OR_SECS));
    return currentHour.isBefore(minHourAllowedForTransactions)
        || currentHour.isAfter(maxHourAllowedForTransactions);
  }

}
