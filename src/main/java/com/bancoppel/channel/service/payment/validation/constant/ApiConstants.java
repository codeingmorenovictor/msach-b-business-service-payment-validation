
/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.constant;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/**
 * Clase que contiene las constantes del API.
 */
@Component
@Getter
public class ApiConstants {

  /**
   * Constant path pattern for bne interceptor.
   */
  public static final String BASE_PATH = "${constants.api.uri.basePath}";
  /**
   * Constant path for getter to consume.
   */
  public static final String HOLIDAY_ENDPOINT = "${constants.api.uri.specificPaths.payment}";
  /**
   * Constant path pattern for bne interceptor logout.
   */
  @Value("${constants.api.uri.interceptorPath}")
  private String interceptorPath;
  /**
   * Constant used to represent the name of the operation that obtains all the accounts of a client.
   */
  public static final String OPERATION_API = "validatePaymentStatus";
  /**
   * Constant that is used to assign login value.
   */
  public static final String VALUELOGIN = "postLogin";
  /**
   * Constant that is used to assign logout value.
   */
  public static final String VALUEDELETELOGIN = "deleteLogin";
  /**
   * Constante utilizada para mostrar un mensaje acerca de un OK.
   */
  public static final String OK = "OK";

  /**
   * Constante utilizada para mostrar un mensaje acerca de una petición mal formada.
   */
  public static final String BAD_REQUEST = "Bad Request";

  /**
   * Constante utilizada para mostrar un mensaje acerca de una petición no autorizada.
   */
  public static final String UNAUTHORIZED = "Unauthorized";

  /**
   * Constante utilizada para mostrar un mensaje acerca de un acceso no configurado apropiadamente.
   */
  public static final String ACCESS_NOT_CONFIGURED = "Access not configured";

  /**
   * Constante utilizada para mostrar un mensaje acerca de recurso que no pudo ser encontrado.
   */
  public static final String RESOURCE_NOT_FOUND = "Resource not found";

  /**
   * Constante utilizada para mostrar un mensaje acerca de un error en la validación de negocio.
   */
  public static final String BUSINESS_VALIDATION_FAILED = "Business validation failed";

  /**
   * Constante utilizada para mostrar un mensaje acerca de un Internal server error.
   */
  public static final String INTERNAL_ERROR = "Internal server error";

  /**
   * Constante utilizada para representar el nombre del header Content-Type.
   */
  public static final String CONTENT_TYPE = "Content-Type";

  /**
   * Constante utilizada para representar el nombre del header Accept.
   */
  public static final String ACCEPT = "Accept";

  /**
   * Constante utilizada para representar el nombre del header sid.
   */
  public static final String SID = "sid";

  /**
   * Constante utilizada para representar el nombre del header uuid.
   */
  public static final String UUID = "uuid";

  /**
   * Constante utilizada para representar el nombre del header deviceId.
   */
  public static final String DEVICE_ID = "deviceId";
  /**
   * Constante utilizada para representar el nombre del header Authorization.
   */
  public static final String AUTHORIZATION = "Authorization";
  /**
   * Constante utilizada para representar el nombre del header client_id.
   */
  public static final String CLIENT_ID = "client_id";
  /**
   * Constante utilizada para representar el nombre del header Accept-Language.
   */
  public static final String ACCEPT_LANGUAGE = "Accept-Language";
  /**
   * Constante utilizada para representar el nombre del header Host.
   */
  public static final String HOST = "Host";
  /**
   * Constante utilizada para representar el nombre del header User-Agent.
   */
  public static final String USER_AGENT = "User-Agent";
  /**
   * Constante utilizada para representar el nombre del header Content-Encoding.
   */
  public static final String CONTENT_ENCODING = "Content-Encoding";
  /**
   * Constante utilizada para representar el nombre del header Content-Language.
   */
  public static final String CONTENT_LANGUAGE = "Content-Language";
  /**
   * Constante utilizada para representar el nombre del header Content-Length.
   */
  public static final String CONTENT_LENGTH = "Content-Length";
  /**
   * Constante utilizada para representar el nombre del header Content-MD5.
   */
  public static final String CONTENT_MD5 = "Content-MD5";
  /**
   * Constante utilizada para representar el nombre del header Accept-Charset.
   */
  public static final String ACCEPT_CHARSET = "Accept-Charset";
  /**
   * Constante utilizada para representar el nombre del header Date.
   */
  public static final String DATE = "Date";
  /**
   * Constante utilizada para representar el nombre del header Accept-Encoding.
   */
  public static final String ACCEPT_ENCODING = "Accept-Encoding";
  /**
   * Constante utilizada para representar el nombre del header ChannelId.
   */
  public static final String CHANNEL_ID = "ChannelId";
  /**
   * Constante utilizada para representar el nombre del header ChannelId.
   */
  public static final String REFRESH_TOKEN = "refresh_token";
  /**
   * Constante utilizada para mostrar el status code 200.
   */
  public static final int CODE_OK = 200;
  /**
   * Constante utilizada para mostrar el status code 400.
   */
  public static final int CODE_BAD_REQUEST = 400;
  /**
   * Constante utilizada para mostrar el status code 401.
   */
  public static final int CODE_UNAUTHORIZED = 401;
  /**
   * Constante utilizada para mostrar el status code 403.
   */
  public static final int CODE_ACCESS_NOT_CONFIGURED = 403;
  /**
   * Constante utilizada para mostrar el status code 404.
   */
  public static final int CODE_RESOURCE_NOT_FOUND = 404;
  /**
   * Constante utilizada para mostrar el status code 422.
   */
  public static final int CODE_BUSINESS_VALIDATION_FAILED = 422;
  /**
   * Constante utilizada para mostrar el status code 500.
   */
  public static final int CODE_INTERNAL_ERROR = 500;

  /**
   * Regex validation expression for application/json value.
   */
  public static final String REGEX = "(?i)" + MediaType.APPLICATION_JSON_VALUE + "|(?i)"
      + MediaType.APPLICATION_JSON_VALUE + "(;charset=+.*)*";
  /**
   * Regex mensaje mostrado cuando el header Accept falla.
   */
  public static final String REGEX_MSG = "Accept header is wrong, must be "
      + MediaType.APPLICATION_JSON_VALUE + " or " + MediaType.APPLICATION_JSON_VALUE + ";charset=*";

  /**
   * Swagger configuration Starts here. Application base package.
   */
  @Value("${constants.swagger.basePackage}")
  private String basePackage;

  /**
   * Swagger title.
   */
  @Value("${constants.swagger.title}")
  private String title;

  /**
   * Application description.
   */
  @Value("${constants.swagger.descriptionApi}")
  private String descriptionApi;

  /**
   * Swagger version.
   */
  @Value("${constants.swagger.version}")
  private String version;

  /**
   * Developer name.
   */
  @Value("${constants.swagger.nameDeveloper}")
  private String nameDeveloper;

  /**
   * Contact URL.
   */
  @Value("${constants.swagger.contactUrl}")
  private String contactUrl;

  /**
   * Developer email.
   */
  @Value("${constants.swagger.emailDeveloper}")
  private String emailDeveloper;

  /**
   * Application label.
   */
  @Value("${constants.swagger.label}")
  private String label;

  /**
   * Application resourceLocation.
   */
  @Value("${constants.swagger.resourceLocation}")
  private String resourceLocation;

  /**
   * Application webjars.
   */
  @Value("${constants.swagger.webjars}")
  private String webjars;

  /**
   * Application webjarsLocation.
   */
  @Value("${constants.swagger.webjarsLocation}")
  private String webjarsLocation;

  /**
   * Constante para obtener los diferentes tipos de canales a validar.
   */
  @Value("${valid.channels}")
  private String validChannels;

  /**
   * Constante para obtener los canales validados.
   */
  @Value("${validate.channels}")
  private String validatedChannels;

  /**
   * Constante para logear el tiempo de la query.
   */
  public static final String QUERY_TIME_MSG_LOG = "QUERIES JPA time: {}";
  /**
   * Constante para logear el tiempo de la query 1.
   */
  public static final String QUERY_TIME1_MSG_LOG = "TIEMPO CONSULTA 1: {} ";
  /**
   * Constante para logear el tiempo de la query 2.
   */
  public static final String QUERY_TIME2_MSG_LOG = "TIEMPO CONSULTA 2: {} ";
  /**
   * Constante para logear el tiempo de la query 3.
   */
  public static final String QUERY_TIME3_MSG_LOG = "TIEMPO CONSULTA 3: {} ";
  /**
   * Constante para logear el tiempo de la query 4.
   */
  public static final String QUERY_TIME4_MSG_LOG = "TIEMPO CONSULTA 4: {} ";
  /**
   * Constante para logear el mensaje de la respuesta de cache.
   */
  public static final String DATA_CACHE_MSG_LOG = "dataCache: {} ";
  /**
   * Constante para logear el mensaje de la respuesta Autenticación.
   */
  public static final String RESPONSE_AUTH_MSG_LOG = "responseAuthenticate: {} ";

  /**
   * Constante para loguear la Logica de negocio para peticion al servicio de dias festivos.
   */
  public static final String LOG_HOLIDAY_SERVICE =
      "Business: getResponseStatusAccordingToCurrentDate";

  /**
   * Constante para el booleano de true.
   */
  public static final boolean TRUE = true;

  /**
   * Constructor privado para evitar la instancia de esta clase.
   */
  private ApiConstants() {

  }

}
