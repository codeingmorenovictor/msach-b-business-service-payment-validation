/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.feign;

import com.bancoppel.channel.service.payment.validation.constant.HolidaysConstants;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.DownstreamException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.HolidayForbiddenAccessException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.HolidayNoDataAccessException;
import com.bancoppel.channel.service.payment.validation.model.holiday.HolidayResponse;
import com.bancoppel.commons.annotation.HandledProcedure;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import java.util.Optional;

/**
 * Interface cliente feign para consultar dias festivos y/o no laborables.
 */
@FeignClient(name = HolidaysConstants.NON_WORKING_DAYS_FEIGN_NAME,
    url = HolidaysConstants.NON_WORKING_DAYS_SERVICE_URL_PATH)
public interface HolidaysFeignClient {


  /**
   * Metodo para hacer consulta de dias festivos y/o no laborables.
   */
  @HandledProcedure(value = HolidaysConstants.GET_HOLIDAYS_SERVICE,
      name = HolidaysConstants.GET_HOLIDAYS_SERVICE,
      ignoreExceptions = {HolidayForbiddenAccessException.class, HolidayNoDataAccessException.class,
          DownstreamException.class})
  @GetMapping(path = HolidaysConstants.NON_WORKING_DAYS_BASE_PATH,
      consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  Optional<HolidayResponse> getHolidays(@RequestHeader HttpHeaders headers);
}
