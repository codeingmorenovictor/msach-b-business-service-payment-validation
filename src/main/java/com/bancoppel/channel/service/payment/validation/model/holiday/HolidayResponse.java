/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.model.holiday;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.List;

/**
 * Clase que representa la estructura del response obtenido del servicio de dias feriados.
 */
@Getter
@Setter
@ToString
public class HolidayResponse {

  /**
   * Campo Fecha actual.
   */
  private String today;
  /**
   * Listado de dias feriados obtenidos en el response.
   */
  private List<HolidayModel> holidays;

}
