/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.component;

import com.bancoppel.bex.redisactions.service.RedisActions;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.constant.SpecialCharacterConstants;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.UnauthorizedException;
import com.bancoppel.channel.service.payment.validation.service.SecurityService;
import com.bancoppel.decrypt.service.Md5OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementación de lógica de validaciones de seguridad.
 *
 * @author Victor Moreno/Nova
 */
@Component
public class SecurityComponent implements SecurityService {

  /**
   * Inyección de la dependencia RedisActions.
   */
  @Autowired
  private RedisActions redisActions;

  /**
   * Inyección de la dependencia MD5OperationService.
   */
  @Autowired
  private Md5OperationService md5OperationService;
  /**
   * Inyección de variables.
   */
  @Autowired
  private ApiConstants apiConstants;

  /**
   * {@inheritDoc}.
   */
  @Override
  public boolean validateRegister(String authorization, String deviceId, String channelId)
      throws NoSuchAlgorithmException {
    if (!channelIsValid(channelId)) {
      throw new UnauthorizedException();
    }

    if (channelNeedsRegister(channelId)) {
      return redisActions.exists(md5OperationService.convertToMd5(authorization.concat(deviceId)));
    }

    return true;
  }

  /**
   * Método utilizado para validar que el channel id recibido es un canal válido.
   * 
   * @param channelId id único de canal
   * @return true si la validación fue correcta
   */
  private boolean channelIsValid(String channelId) {
    Set<String> validChannels = new HashSet<>(
        Arrays.asList(apiConstants.getValidChannels().split(SpecialCharacterConstants.COMMA)));
    return validChannels.stream().anyMatch(channel -> channel.equals(channelId));
  }

  /**
   * Método utilizado para verificar si el channel id recibido requiere registro activo.
   * 
   * @param channelId id único de canal
   * @return true si la validación fue correcta
   */
  private boolean channelNeedsRegister(String channelId) {
    Set<String> validatedChannels = new HashSet<>(
        Arrays.asList(apiConstants.getValidatedChannels().split(SpecialCharacterConstants.COMMA)));
    return validatedChannels.stream().anyMatch(channel -> channel.equals(channelId));
  }
}
