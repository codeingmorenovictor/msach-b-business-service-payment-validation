/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.constant;

import org.springframework.stereotype.Component;

@Component
public class HolidaysConstants {

  /**
   * Constante de nombre del servicio consumido para conocer dias festivos
   */
  public static final String NON_WORKING_DAYS_FEIGN_NAME = "msaxd-d-domain-holiday-query";
  /**
   * URL del servicio consumido para conocer dias festivos
   */
  public static final String NON_WORKING_DAYS_SERVICE_URL_PATH =
      "${constants.api.feign.nonWorkingDays.serviceURL.basePath}";
  /**
   * Path consumido para conocer dias festivos
   */
  public static final String NON_WORKING_DAYS_BASE_PATH =
      "${constants.api.feign.nonWorkingDays.basePath}";
  /**
   * LOG para localizar tiempo de consumo para obtener el numero de cliente del SSO.
   */
  public static final String GET_HOLIDAYS_SERVICE = "Getting data from Holidays service";



  /**
   * Constante que indica el horario de cierre de operaciones de la app.
   */
  public static final Integer MAX_HOUR = 22;
  /**
   * Constante que indica el horario de inicio de operaciones de la app.
   */
  public static final Integer MIN_HOUR = 8;
  /**
   * Constante que indica el valor cero para minutos y segundos en los LocalTime.
   */
  public static final String ZERO_VAL_FOR_MIN_OR_SECS = "00";



  /**
   * Constante que determina posición inicial para susbstring mes/día para dias no laborables.
   */
  public static final Integer HOLIDAY_SUBSTRING_START = 5;
  /**
   * Constante que determina posición final para susbstring mes/día para dias no laborables.
   */
  public static final Integer HOLIDAY_SUBSTRING_END = 10;

  /**
   * Constante que representa el valor mes/día correspondiente al 1 de Enero.
   */
  public static final String JANUARY_FIRST = "${constants.api.nonWorkable.januaryFirst}";

  /**
   * Constante que representa el valor mes/día correspondiente al 25 de diciembre.
   */
  public static final String DECEMBER_TWENTYFIVE =
      "${constants.api.nonWorkable.decemberTwentyFive}";

  /**
   * Private constructor will prevent the instantiation of this class.
   */


  private HolidaysConstants() {}

}
