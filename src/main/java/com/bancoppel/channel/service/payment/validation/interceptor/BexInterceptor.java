/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.interceptor;

import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.constant.Constants;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.ForbiddenException;
import com.bancoppel.channel.service.payment.validation.service.SecurityService;
import com.bancoppel.channel.service.payment.validation.util.Util;
import com.bancoppel.commons.annotation.ValidateHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Clase que se ejecuta cuando inicia la petición del controlador.
 */
@Component
public class BexInterceptor extends HandlerInterceptorAdapter { 

  /**
   * Variable para logeo.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(BexInterceptor.class);

  /**
   * Servicio donde radican las validaciones de seguridad.
   */
  @Autowired
  private SecurityService securityService;

  /**
   * Método para configurar el MDC y guardar el tiempo inicial de la petición.
   * 
   * @throws NoSuchAlgorithmException - NoSuchAlgorithmException
   */
  @ValidateHeaders
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws ServletRequestBindingException, NoSuchAlgorithmException {
    long t0 = System.currentTimeMillis();
    if (!securityService.validateRegister(request.getHeader(ApiConstants.AUTHORIZATION),
        request.getHeader(ApiConstants.DEVICE_ID), request.getHeader(ApiConstants.CHANNEL_ID))) {
      throw new ForbiddenException();
    }
    request.setAttribute(Constants.T0_REQ_ATTRIBUTE, t0);

    MDC.put(Constants.UUID_MDC_LABEL, request.getHeader(ApiConstants.UUID));

    Util.printHeaders(request);

    return true;
  }

  /**
   * Calcula el total del tiempo de la petición y limpia el MDC.
   */
  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, Exception ex) {
    long t0 = (Long) request.getAttribute(Constants.T0_REQ_ATTRIBUTE);
    long timeElapsed = System.currentTimeMillis() - t0;
    LOGGER.info(Constants.TIME_ELAPSED_MESSAGE, request.getRequestURI(), timeElapsed);

    request.removeAttribute(Constants.T0_REQ_ATTRIBUTE);

    MDC.clear();
  }

}
