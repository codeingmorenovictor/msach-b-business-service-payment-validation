/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.service;

import java.security.NoSuchAlgorithmException;
/**
 * 
 * @author Victor Moreno/Nova
 *
 */
public interface SecurityService {
  
  /**
   * Método utilizado para validar que exista un registro activo.
   * 
   * @param authorization token de autorización
   * @param deviceId identificador único del dispositivo
   * @param channelId identificador único del canal
   * @return booleano que indica si la validación fue exitosa
   * @throws NoSuchAlgorithmException excepción arrojada cuando no existe el algoritmo
   */
  boolean validateRegister(String authorization, String deviceId, String channelId)
      throws NoSuchAlgorithmException;

}
