/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.feign.HolidaysFeignClient;
import com.bancoppel.channel.service.payment.validation.service.HolidaysService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.Optional;

/**
 * Clase test del controlador HolidaysController.
 *
 * @author Victor Moreno/ Nova.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class HolidaysControllerTest {

  public static final String ACCEPT_HEADER = "accept";
  public static final String AUTHORIZATION_HEADER = "789798778887ui";
  public static final String REFRESH_TOKEN_HEADER = "refreshToken";
  public static final String CHANNEL_ID_HEADER = "BEX";
  public static final String UUID_HEADER = "{($randomUUID)}";
  public static final String DEVICE_ID_HEADER = "50";
  public static final String CONTENT_TYPE_HEADER = "application/json";


  /**
   * Variable que representa el controller HolidaysController.
   */
  @InjectMocks
  private HolidaysController holidaysController;
  /**
   * Variable que representa el service HolidaysService.
   */
  @Mock
  private HolidaysService holidaysService;

  /**
   * Objeto mock del cliente Feign para consulta de dias feriados.
   */
  @Mock
  private HolidaysFeignClient holidayClient;

  HttpHeaders headers = new HttpHeaders();

  /**
   * Inicializa los mocks.
   */
  @Before
  public void setUp() {
    headers.add(HttpHeaders.ACCEPT, ACCEPT_HEADER);
    headers.add(HttpHeaders.AUTHORIZATION, AUTHORIZATION_HEADER);
    headers.add(ApiConstants.REFRESH_TOKEN, REFRESH_TOKEN_HEADER);
    headers.add(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_HEADER);
    headers.add(ApiConstants.CHANNEL_ID, CHANNEL_ID_HEADER);
    headers.add(ApiConstants.UUID, UUID_HEADER);
    headers.add(ApiConstants.DEVICE_ID, DEVICE_ID_HEADER);
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Valida disponibilidad del servicio de HolidayBank.
   */
  @Test
  public void validateHolidayBankServiceAvailability() {
    when(holidayClient.getHolidays(headers)).thenReturn(Optional.empty());
    assertEquals(holidayClient.getHolidays(headers), Optional.empty());
  }

  /**
   * Given la petición al microservicio, When valida y no es un dia festivo o fuera de horario, Then
   * regresa un ResponseEntity con status 200.
   */
  @Test
  public void validateHolidayBankResponseOKTest() {
    assertEquals(new ResponseEntity<>(HttpStatus.OK),
        holidaysController.getHolidayStatus(ACCEPT_HEADER, AUTHORIZATION_HEADER,
            REFRESH_TOKEN_HEADER, CONTENT_TYPE_HEADER, CHANNEL_ID_HEADER, UUID_HEADER,
            DEVICE_ID_HEADER));
  }

}
