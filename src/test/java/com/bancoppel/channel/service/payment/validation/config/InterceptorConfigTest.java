/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bancoppel.channel.service.payment.validation.config;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

/**
 * Class that defines the methods to test the InterceptorConfig class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class InterceptorConfigTest {

  /**
   * Component that will be tested.	
   */
  @InjectMocks
  private InterceptorConfig interceptorConfig;
  
  /**
   * Mocked ApiConstants reference.
   */
  @Mock
  private ApiConstants apiConstants;

  /**
   * Test for InterceptoConfig class.
   * 
   * Given an InterceptorRegistry, InterceptorRegistration.
   * When the addInterceptors is invoked.
   * Then simulate add an HandlerInterceptorAdapter.
   * 
   */
  @Test
  public void addInterceptorsTest() {
    InterceptorRegistry registry = mock(InterceptorRegistry.class);
    InterceptorRegistration values = mock(InterceptorRegistration.class);
    Mockito.when(registry.addInterceptor(Mockito.any())).thenReturn(values);

    interceptorConfig.addInterceptors(registry);
    assertNotNull(registry);
  }
  
}
