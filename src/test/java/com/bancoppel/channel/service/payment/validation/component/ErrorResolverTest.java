/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.component;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.constant.Constants;
import com.bancoppel.channel.service.payment.validation.constant.ErrorResolverConstants;
import com.bancoppel.channel.service.payment.validation.constant.SpecialCharacterConstants;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.BadRequestException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.DownstreamException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.ForbiddenException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.HolidayForbiddenAccessException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.HolidayNoDataAccessException;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.UnauthorizedException;
import com.bancoppel.commons.exceptions.CheckHeadersException;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.servlet.NoHandlerFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

/**
 * Class that defines the methods to test the ErrorResolverTest class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ErrorResolverTest {

  /**
   * Injected component that creates an instance of ResponseFormatter with the mock beans.
   */
  @InjectMocks
  private ErrorResolver errorResolver;

  /**
   * Mock component to get information of a file properties.
   */
  @MockBean
  private ErrorResolverConstants errorResolverConstants;



  /**
   * Initialization of variables to simulate information to be tested.
   */
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test to resolve a BadRequestException.
   * 
   * Given the objects HttpServletRequest, BadRequestException and ConstantsConfig requests, When a
   * 'BadRequestException' exception has been thrown, then resolve a BadRequestException.
   */
  @Test
  public void resolveBadRequestExceptionTest() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);

    BadRequestException ex = Mockito.mock(BadRequestException.class);
    List<String> list = new ArrayList<>();
    list.add(SpecialCharacterConstants.EMPTY_STRING);
    Mockito.when(ex.getBadFields()).thenReturn(list);
    errorResolver.resolveBadRequestException(req, ex);
    assertNotNull(errorResolver.resolveBadRequestException(req, ex));

  }

  /**
   * Test to resolve a BadRequestException.
   * 
   * Given the objects HttpServletRequest, BadRequestException and ConstantsConfig requests, When a
   * 'BadRequestException' exception has been thrown, then resolve a BadRequestException.
   */
  @Test
  public void resolveBadRequestExceptionTestBadFieldsNull() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);

    BadRequestException ex = Mockito.mock(BadRequestException.class);
    Mockito.when(ex.getBadFields()).thenReturn(null);
    errorResolver.resolveBadRequestException(req, ex);
    assertNull(ex.getBadFields());
  }

  /**
   * Test to resolve a ConstraintViolationException.
   * 
   * Given the objects HttpServletRequest and ConstraintViolationException and ConstantsConfig
   * requests, When a 'ConstraintViolationException' exception has been thrown, then resolve a
   * ConstraintViolationException.
   */
  @Test
  public void resolveConstraintViolationException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    ConstraintViolationException exception = Mockito.mock(ConstraintViolationException.class);

    errorResolver.resolveConstraintViolation(req, exception);
    assertNotNull(errorResolver.resolveConstraintViolation(req, exception));
  }


  /**
   * Test to resolve a Exception.
   * 
   * Given the objects HttpServletRequest and Exception requests, When a 'Exception' exception has
   * been thrown, then resolve a Exception.
   */
  @Test
  public void resolveException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    Exception ex = Mockito.mock(Exception.class);
    errorResolver.resolveException(req, ex);
    assertNotNull(errorResolver.resolveException(req, ex));
  }

  /**
   * Test to resolve a HttpMediaTypeNotAcceptableException.
   * 
   * Given the objects HttpServletRequest and HttpMediaTypeNotAcceptableException requests, When a
   * 'HttpMediaTypeNotAcceptableException' exception has been thrown, then resolve a
   * HttpMediaTypeNotAcceptableException.
   */
  @Test
  public void resolveHttpMediaTypeNotAcceptableException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    HttpMediaTypeNotAcceptableException ex =
        Mockito.mock(HttpMediaTypeNotAcceptableException.class);
    errorResolver.resolveHttpMediaTypeNotAcceptableException(req, ex);
    assertNotNull(errorResolver.resolveHttpMediaTypeNotAcceptableException(req, ex));
  }

  /**
   * Test to resolve a HttpMediaTypeNotAcceptableException.
   * 
   * Given the objects HttpServletRequest and HttpMediaTypeNotAcceptableException requests, When a
   * 'HttpMediaTypeNotAcceptableException' exception has been thrown, then resolve a
   * HttpMediaTypeNotAcceptableException.
   */
  @Test
  public void resolveHttpMediaTypeNotSupportedException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    HttpMediaTypeNotSupportedException ex = Mockito.mock(HttpMediaTypeNotSupportedException.class);
    errorResolver.resolveHttpMediaTypeNotSupportedException(req, ex);
    assertNotNull(errorResolver.resolveHttpMediaTypeNotSupportedException(req, ex));
  }

  /**
   * Test to resolve a HttpMessageNotReadableException.
   * 
   * Given the objects HttpServletRequest and HttpMessageNotReadableException requests, When a
   * 'HttpMessageNotReadableException' exception has been thrown, then resolve a
   * HttpMessageNotReadableException.
   */
  @Test
  public void resolveHttpMessageNotReadableException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    HttpMessageNotReadableException exception = Mockito.mock(HttpMessageNotReadableException.class);
    Mockito.when(exception.getMessage())
        .thenReturn(Constants.ACCOUNT_TYPE_STRING + SpecialCharacterConstants.SPACE_STRING
            + Constants.JSON_STRING + SpecialCharacterConstants.COLON
            + SpecialCharacterConstants.EMPTY_STRING);

    errorResolver.resolveHttpMessageNotReadableException(req, exception);
    assertNotNull(errorResolver.resolveHttpMessageNotReadableException(req, exception));
  }

  /**
   * Test to resolve a HttpMessageNotReadableException.
   * 
   * Given the objects HttpServletRequest and HttpMessageNotReadableException requests, When a
   * 'HttpMessageNotReadableException' exception has been thrown, then resolve a
   * HttpMessageNotReadableException.
   */
  @Test
  public void resolveHttpMessageNotReadableExceptionMessageNull() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    Mockito.when(errorResolverConstants.getGenericErrorDescription())
        .thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    HttpMessageNotReadableException exception = Mockito.mock(HttpMessageNotReadableException.class);
    Mockito.when(exception.getMessage()).thenReturn(null);

    errorResolver.resolveHttpMessageNotReadableException(req, exception);
    assertNotNull(errorResolver.resolveHttpMessageNotReadableException(req, exception));
  }

  /**
   * Test to resolve a HttpRequestMethodNotSupportedException.
   * 
   * Given the objects HttpServletRequest and HttpRequestMethodNotSupportedException requests, When
   * a 'HttpRequestMethodNotSupportedException' exception has been thrown, then resolve a
   * HttpRequestMethodNotSupportedException.
   */
  @Test
  public void resolveHttpRequestMethodNotSupportedException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    HttpRequestMethodNotSupportedException exception =
        Mockito.mock(HttpRequestMethodNotSupportedException.class);

    errorResolver.resolveHttpRequestMethodNotSupportedException(req, exception);
    assertNotNull(errorResolver.resolveHttpRequestMethodNotSupportedException(req, exception));
  }

  /**
   * Test to resolve a HystrixRuntimeException.
   * 
   * Given the objects HttpServletRequest and HystrixRuntimeException requests, When a
   * 'HystrixRuntimeException' exception has been thrown, then resolve a HystrixRuntimeException.
   */
  @Test
  public void resolveHystrixRuntimeException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    HystrixRuntimeException exception = Mockito.mock(HystrixRuntimeException.class);

    errorResolver.resolveHystrixRuntimeException(req, exception);
    assertNotNull(errorResolver.resolveHystrixRuntimeException(req, exception));
  }

  /**
   * Test to resolve a MethodArgumentNotValidException.
   * 
   * Given the objects HttpServletRequest and MethodArgumentNotValidException requests, When a
   * 'MethodArgumentNotValidException' exception has been thrown, then resolve a
   * MethodArgumentNotValidException.
   */
  @Test
  public void resolveMethodArgumentNotValidException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    MethodArgumentNotValidException exception = Mockito.mock(MethodArgumentNotValidException.class);

    List<FieldError> fieldErrors = new ArrayList<>();
    FieldError fieldError = Mockito.mock(FieldError.class);
    fieldErrors.add(fieldError);

    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    Mockito.when(exception.getBindingResult()).thenReturn(bindingResult);
    Mockito.when(exception.getBindingResult().getFieldErrors()).thenReturn(fieldErrors);

    errorResolver.resolveMethodArgumentNotValidException(req, exception);
    assertNotNull(errorResolver.resolveMethodArgumentNotValidException(req, exception));
  }

  /**
   * Test to resolve a NoHandlerFoundException.
   * 
   * Given the objects HttpServletRequest and NoHandlerFoundException requests, When a
   * 'NoHandlerFoundException' exception has been thrown, then resolve a NoHandlerFoundException.
   */
  @Test
  public void resolveNoHandlerFoundException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    NoHandlerFoundException exception = Mockito.mock(NoHandlerFoundException.class);

    errorResolver.resolveNoHandlerFoundException(req, exception);
    assertNotNull(errorResolver.resolveNoHandlerFoundException(req, exception));
  }


  /**
   * Test to resolve a ServletRequestBindingException.
   * 
   * Given the objects HttpServletRequest and ServletRequestBindingException requests, When a
   * 'ServletRequestBindingException' exception has been thrown, then resolve a
   * ServletRequestBindingException.
   */
  @Test
  public void resolveServletRequestBindingException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    ServletRequestBindingException exception = Mockito.mock(ServletRequestBindingException.class);
    Mockito.when(exception.getMessage()).thenReturn(ApiConstants.OK);

    errorResolver.resolveServletRequestBindingException(req, exception);
    assertNotNull(errorResolver.resolveServletRequestBindingException(req, exception));
  }

  /**
   * Test to resolve a UnAuthorizedException.
   * 
   * Given the objects HttpServletRequest and UnAuthorizedException requests, When a
   * 'UnAuthorizedException' exception has been thrown, then resolve a UnAuthorizedException.
   */
  @Test
  public void resolveUnAuthorizedException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    UnauthorizedException exception = Mockito.mock(UnauthorizedException.class);

    errorResolver.resolveUnAuthorizedException(req, exception);
    assertNotNull(errorResolver.resolveUnAuthorizedException(req, exception));
  }

  /**
   * Test to resolve a DownstreamException.
   * 
   * Given the objects HttpServletRequest and DownstreamException and ConstantsConfig requests, When
   * a 'DownstreamException' exception has been thrown, then resolve a DownstreamException.
   */
  @Test
  public void resolveDownstreamException() {

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    DownstreamException ex = Mockito.mock(DownstreamException.class);
    HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);

    assertNotNull(errorResolver.resolveDownstreamException(req, resp, ex));
  }

  /**
   * Test to resolve a NotValidHeadersException.
   * 
   * Given the objects HttpServletRequest and NotValidHeadersException and ConstantsConfig requests,
   * When a 'NotValidHeadersException' exception has been thrown, then resolve a
   * NotValidHeadersException.
   */
  @Test
  public void resolveNotValidHeadersException() {
    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    CheckHeadersException ex = Mockito.mock(CheckHeadersException.class);
    assertNotNull(errorResolver.resolveCheckHeadersException(req, ex));
  }


  /**
   * Prueba HolidayForbiddenAccessException. * * Given Un HttpServletRequest y
   * HolidayForbiddenAccessException. * When Es lanzada un excepcion
   * HolidayForbiddenAccessException. * Then Resuelve HolidayForbiddenAccessException.
   */
  @Test
  public void resolveHolidayForbiddenAccessException() {
    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse res = Mockito.mock(HttpServletResponse.class);
    HolidayForbiddenAccessException exception = Mockito.mock(HolidayForbiddenAccessException.class);
    errorResolver.holidayApiForbiddenException(req, res, exception);
    assertNotNull(errorResolver.holidayApiForbiddenException(req, res, exception));
  }

  /**
   * Prueba HolidayNoDataAccessException. * * Given Un HttpServletRequest y
   * HolidayNoDataAccessException. * When Es lanzada un excepcion HolidayNoDataAccessException. *
   * Then Resuelve HolidayNoDataAccessException.
   */
  @Test
  public void resolveHolidayNoDataAccessException() {
    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse res = Mockito.mock(HttpServletResponse.class);
    HolidayNoDataAccessException exception = Mockito.mock(HolidayNoDataAccessException.class);
    errorResolver.holidayApiNoDataException(req, res, exception);
    assertNotNull(errorResolver.holidayApiNoDataException(req, res, exception));
  }

  /**
   * Prueba ForbiddenException. * * Given Un HttpServletRequest y ForbiddenException. * When Es
   * lanzada un excepcion ForbiddenException. * Then Resuelve ForbiddenException.
   */
  @Test
  public void resolveForbiddenException() {
    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    ForbiddenException exception = Mockito.mock(ForbiddenException.class);
    errorResolver.resolveForbiddenException(req, exception);
    assertNotNull(errorResolver.resolveForbiddenException(req, exception));
  }


}
