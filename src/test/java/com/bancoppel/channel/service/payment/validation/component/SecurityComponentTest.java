/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.component;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import com.bancoppel.bex.redisactions.service.RedisActions;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.constant.SpecialCharacterConstants;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.UnauthorizedException;
import com.bancoppel.decrypt.service.Md5OperationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.security.NoSuchAlgorithmException;


/**
 * Clase de prueba de validaciones de seguridad.
 *
 * @author Victor Moreno/Nova
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class SecurityComponentTest {

  /**
   * Clase a testear.
   */
  @InjectMocks
  private SecurityComponent securityComponent;

  /**
   * Mock de la dependencia RedisActions.
   */
  @Mock
  private RedisActions redisActions;

  /**
   * Inyección de la dependencia MD5OperationService.
   */
  @Mock
  private Md5OperationService md5OperationService;
  /**
   * Mock de api values.
   */
  @Mock
  private ApiConstants apiConstants;

  /**
   * Método utilizado para inicializar los mocks.
   */
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test unitario del flujo en el que se tiene un canal válido y necesita registro activo.
   * 
   * @throws NoSuchAlgorithmException excepción lanzada cuando no existe el algoritmo
   */
  @Test
  public void testValidateRegisterTrue() throws NoSuchAlgorithmException {
    when(apiConstants.getValidChannels()).thenReturn("Valid channels retrieved successfully");
    when(apiConstants.getValidatedChannels()).thenReturn("BEX channels retrieved successfully");
    when(md5OperationService.convertToMd5(Mockito.anyString()))
        .thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    when(redisActions.exists(Mockito.anyString())).thenReturn(true);
    assertTrue(securityComponent.validateRegister(SpecialCharacterConstants.EMPTY_STRING,
        SpecialCharacterConstants.EMPTY_STRING, apiConstants.getValidChannels()));
  }

  /**
   * Test unitario del flujo en el que se tiene un canal válido y no necesita registro activo.
   * 
   * @throws NoSuchAlgorithmException excepción lanzada cuando no existe el algoritmo
   */
  @Test
  public void testValidateRegisterDoesNotNeedRegister() throws NoSuchAlgorithmException {
    when(apiConstants.getValidChannels()).thenReturn("Valid channels retrieved successfully");
    when(apiConstants.getValidatedChannels()).thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    when(md5OperationService.convertToMd5(Mockito.anyString()))
        .thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    when(redisActions.exists(Mockito.anyString())).thenReturn(true);
    assertTrue(securityComponent.validateRegister(SpecialCharacterConstants.EMPTY_STRING,
        SpecialCharacterConstants.EMPTY_STRING, apiConstants.getValidChannels()));
  }

  /**
   * Test unitario del flujo en el que se tiene un canal inválido.
   * 
   * @throws NoSuchAlgorithmException excepción lanzada cuando no existe el algoritmo
   */
  @Test(expected = UnauthorizedException.class)
  public void testValidateRegisterChannelNotValid() throws NoSuchAlgorithmException {
    when(apiConstants.getValidChannels()).thenReturn("Valid channels retrieved successfully");
    when(apiConstants.getValidatedChannels()).thenReturn("BEX channels retrieved successfully");
    when(md5OperationService.convertToMd5(Mockito.anyString()))
        .thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    when(redisActions.exists(Mockito.anyString())).thenReturn(true);
    securityComponent.validateRegister(SpecialCharacterConstants.EMPTY_STRING,
        SpecialCharacterConstants.EMPTY_STRING, "");
  }

}
