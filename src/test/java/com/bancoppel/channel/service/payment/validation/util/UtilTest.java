/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bancoppel.channel.service.payment.validation.util;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Test;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * Class that defines the methods to test the Util class.
 */
public class UtilTest {
  
  /**
   * Constant that is used to show the label 'TEST'
   */
  private static final String TEST_STRING = "TEST";
  
  /**
   * Constant that is used use JSON string
   */
  private static final String TEST_JSON_STRING = "{ \"name\" : \"api\" }";

  /**
   * Test getJsonTest.
   * 
   * Given a Object requests,
   * When the method getJson is invoked,
   * Then return a string with json format of the object.
   */
  @Test
  public void getJsonTest() {
	  assertNotNull(Util.getJson(TEST_STRING));
  }
  
  /**
   * Test printHeadersNullTest.
   * 
   * Given a HttpServletRequest requests,
   * When the method printHeaders is invoked,
   * Then logging the name of each header.
   */
  @Test
  public void printHeadersNullTest() {
    Enumeration<String> headerNames = null;
    
    HttpServletRequest req = mock(HttpServletRequest.class);
    when(req.getHeaderNames()).thenReturn(headerNames);
    
    Util.printHeaders(req);
    assertNotNull(req);
  }
  
  /**
   * Test printHeadersWithHearderNamesTest.
   * 
   * Given a HttpServletRequest requests,
   * When the method printHeaders is invoked with hearder names,
   * Then logging the name of each header.
   */
  @Test
  public void printHeadersWithHearderNamesTest() {
    Map<String, String> headers = new HashMap<>();
    headers.put(null, "HTTP/1.1 200 OK");
    headers.put("Content-Type", "text/html");

    Iterator<String> iterator = headers.keySet().iterator();
    Enumeration<String> headerNames = new Enumeration<String>() {
        @Override
        public boolean hasMoreElements() {
            return iterator.hasNext();
        }

        @Override
        public String nextElement() {
            return iterator.next();
        }
    };
    
    HttpServletRequest req = mock(HttpServletRequest.class);
    when(req.getHeaderNames()).thenReturn(headerNames);
    
    Util.printHeaders(req);
    assertNotNull(req);
  }
  
  /**
   * Test printHeadersWithHearderNamesAndHeaderValuesTest.
   * 
   * Given a HttpServletRequest requests,
   * When the method printHeaders is invoked with hearder names 
   * and header values,
   * Then logging the name of each header.
   */
  @Test
  public void printHeadersWithHearderNamesAndHeaderValuesTest() {
    Map<String, String> headers = new HashMap<>();
    headers.put("sid", "e4041f30-aeca-4ffc-91b7-015ab677693b");
    headers.put("uuid", "uuid1f30-aeca-4ffc-91b7-015ab677693b");
    headers.put("Content-Type", "text/html");

    Iterator<String> namesIterator = headers.keySet().iterator();
    Enumeration<String> headerNames = new Enumeration<String>() {
        @Override
        public boolean hasMoreElements() {
            return namesIterator.hasNext();
        }

        @Override
        public String nextElement() {
            return namesIterator.next();
        }
    };
    
    Iterator<String> valuesIterator = headers.keySet().iterator();
    Enumeration<String> headerValues = new Enumeration<String>() {
        @Override
        public boolean hasMoreElements() {
            return valuesIterator.hasNext();
        }

        @Override
        public String nextElement() {
            return valuesIterator.next();
        }
    };
    
    HttpServletRequest req = mock(HttpServletRequest.class);
    when(req.getHeaderNames()).thenReturn(headerNames);
    when(req.getHeaders(any())).thenReturn(headerValues);
    
    Util.printHeaders(req);
    assertNotNull(headerValues);
  }
  
  /**
   * Test convertStringToDataCache.
   * 
   * Given a Object requests, When the method getJson is invoked, Then return a string with json
   * format of the object.
   */
  @Test
  public void convertStringToDataCacheExTest() {
    assertNotNull(Util.convertStringToDataCache(TEST_STRING));
  }
  
  /**
   * Test convertStringToDataCache.
   * 
   * Given a Object requests, When the method getJson is invoked, Then return a string with json
   * format of the object.
   */
  @Test
  public void convertStringToDataCacheTest() {
    assertNotNull(Util.convertStringToDataCache(TEST_JSON_STRING));
  }

}
