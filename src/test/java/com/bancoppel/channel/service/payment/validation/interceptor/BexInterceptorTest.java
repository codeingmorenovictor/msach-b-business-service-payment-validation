/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.interceptor;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.ForbiddenException;
import com.bancoppel.channel.service.payment.validation.service.SecurityService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.HttpMethod;

/**
 * Class that defines the methods to test the BneInterceptor class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class BexInterceptorTest {

  /**
   * Constant that is used as a key to store the initial time of the request.
   */
  private static final String T0_REQ_ATTRIBUTE = "req.t0";
  /**
   * Constant that is used to simulate the session id.
   */
  private static final String SID = "ee016151-3291-41ef-891b-044a0b18e70e";
  /**
   * Constant that is used to simulate an uuid.
   */
  private static final String UUID = "0643a8dc-6ccb-4734-9e42-62532f2502b5";
  /**
   * Constant that is used to show the label 'application/json'.
   */
  private static final String APPLICATION_JSON = "application/json";
  /**
   * Constant that is used to simulate an ACCEPT_LANGUAGE.
   */
  private static final String ACCEPT_LANGUAGE = "es";
  /**
   * Constant that is used to simulate a CHANNEL_ID.
   */
  private static final String CHANNEL_ID = "17091201";
  /**
   * Constant that is used to simulate a CLIENT_ID.
   */
  private static final String CLIENT_ID = "0";
  /**
   * Constant that is used to simulate a AUTHORIZATION.
   */
  private static final String AUTHORIZATION = "0";
  /**
   * Constant that is used to show the label 'ID_CANAL'
   */
  private static final String ID_CANAL_LABEL = "ID_CANAL";
  /**
   * Constant used to represent the uri of the api.
   */
  private static final String SEARCH_API_URI = "/api/private/v1/channels/bne/accounts/search";
  /**
   * Constant used to represent the uri of the api.
   */
  private static final String PASSWORD_API_URI = "/api/v1/authorization/password";

  /**
   * Component that will be tested.
   */
  @InjectMocks
  private BexInterceptor bexInterceptor = new BexInterceptor();

  @Mock
  private SecurityService securitySvc;
  /**
   * This parameter is used to create the request of the api.
   */
  private MockHttpServletRequest mockHttpServletRequest;
  /**
   * This parameter is used to create the response of the api.
   */
  private MockHttpServletResponse mockHttpServletResponse;

  /**
   * This method fills all the fields necessary to simulate the operations of the Session
   * Interceptor.
   */
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    mockHttpServletRequest = new MockHttpServletRequest();
    mockHttpServletRequest.addHeader(ApiConstants.SID, SID);
    mockHttpServletRequest.addHeader(ApiConstants.ACCEPT, APPLICATION_JSON);
    mockHttpServletRequest.addHeader(ApiConstants.UUID, UUID);
    mockHttpServletRequest.addHeader(ApiConstants.DEVICE_ID, CHANNEL_ID);
    long t0 = System.currentTimeMillis();
    mockHttpServletRequest.setAttribute(T0_REQ_ATTRIBUTE, t0);
    mockHttpServletRequest.setMethod(HttpMethod.POST);
    mockHttpServletRequest.setContentType(APPLICATION_JSON);
    mockHttpServletRequest.setRequestURI(SEARCH_API_URI);

    mockHttpServletResponse = new MockHttpServletResponse();
  }

  /**
   * 
   * Given MockHttpServletRequest and UserSessionRequestService. When the interceptor proccess
   * (preHandle, afterCompletion) is invoked, Then the proccess is ok.
   * 
   * @throws Exception excepción genérica
   */
  @Test
  public void interceptorTest() throws Exception {
    Map<String, String> cacheMap = new HashMap<>();
    cacheMap.put(ID_CANAL_LABEL, CHANNEL_ID);

    mockHttpServletRequest = new MockHttpServletRequest();
    mockHttpServletRequest.addHeader(ApiConstants.SID, SID);
    mockHttpServletRequest.addHeader(ApiConstants.DEVICE_ID, CHANNEL_ID);
    mockHttpServletRequest.addHeader(ApiConstants.CLIENT_ID, CLIENT_ID);
    mockHttpServletRequest.addHeader(ApiConstants.AUTHORIZATION, AUTHORIZATION);
    mockHttpServletRequest.addHeader(ApiConstants.ACCEPT_LANGUAGE, ACCEPT_LANGUAGE);
    mockHttpServletRequest.addHeader(ApiConstants.CONTENT_TYPE, APPLICATION_JSON);
    mockHttpServletRequest.addHeader(ApiConstants.UUID, UUID);
    mockHttpServletRequest.addHeader(ApiConstants.DEVICE_ID, StringUtils.EMPTY);
    mockHttpServletRequest.addHeader(ApiConstants.CHANNEL_ID, StringUtils.EMPTY);
    mockHttpServletRequest.setContentType(APPLICATION_JSON);
    mockHttpServletRequest.addHeader(ApiConstants.ACCEPT, APPLICATION_JSON);
    mockHttpServletRequest.setMethod(HttpMethod.POST);
    mockHttpServletRequest.setRequestURI(PASSWORD_API_URI);
    when(
        securitySvc.validateRegister(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
            .thenReturn(true);
    bexInterceptor.preHandle(mockHttpServletRequest, mockHttpServletResponse, null);
    Exception ex = new Exception();
    bexInterceptor.afterCompletion(mockHttpServletRequest, mockHttpServletResponse, null, ex);
    assertNotNull(bexInterceptor);
  }

  /**
   * 
   * Given MockHttpServletRequest and UserSessionRequestService. When the interceptor proccess
   * (preHandle, afterCompletion) is invoked, Then the proccess is ok.
   * 
   * @throws Exception excepción genérica
   */
  @Test(expected = ForbiddenException.class)
  public void interceptorTestException() throws Exception {
    Map<String, String> cacheMap = new HashMap<>();
    cacheMap.put(ID_CANAL_LABEL, CHANNEL_ID);

    mockHttpServletRequest = new MockHttpServletRequest();

    when(
        securitySvc.validateRegister(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
            .thenReturn(false);
    bexInterceptor.preHandle(mockHttpServletRequest, mockHttpServletResponse, null);
  }

}
