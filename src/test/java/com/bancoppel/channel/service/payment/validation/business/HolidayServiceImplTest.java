/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.channel.service.payment.validation.business;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import com.bancoppel.channel.service.payment.validation.component.ErrorResolver;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.HolidayNoDataAccessException;
import com.bancoppel.channel.service.payment.validation.feign.HolidaysFeignClient;
import com.bancoppel.channel.service.payment.validation.model.holiday.HolidayModel;
import com.bancoppel.channel.service.payment.validation.model.holiday.HolidayResponse;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Clase test de la implementacion HolidayServiceImplTest.
 *
 * @author Victor Moreno/ Nova.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class HolidayServiceImplTest {

  @InjectMocks
  private HolidayServiceImpl holidayServiceImpl;
  /**
   * Injected component that creates an instance of ResponseFormatter with the mock beans.
   */
  @InjectMocks
  private ErrorResolver errorResolver;
  @Mock
  private HolidaysFeignClient holidaysFeignClient;

  public HttpHeaders getHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.ACCEPT, StringUtils.EMPTY);
    headers.add(HttpHeaders.AUTHORIZATION, StringUtils.EMPTY);
    headers.add(ApiConstants.REFRESH_TOKEN, StringUtils.EMPTY);
    headers.add(HttpHeaders.CONTENT_TYPE, StringUtils.EMPTY);
    headers.add(ApiConstants.CHANNEL_ID, StringUtils.EMPTY);
    headers.add(ApiConstants.UUID, StringUtils.EMPTY);
    headers.add(ApiConstants.DEVICE_ID, StringUtils.EMPTY);
    return headers;
  }


  @Test(expected = HolidayNoDataAccessException.class)
  public void validateHolidayNoDataAccessThrowsException() {
    Optional<HolidayResponse> holidayResponse = Optional.ofNullable(null);
    Mockito.when(holidaysFeignClient.getHolidays(Mockito.any())).thenReturn(holidayResponse);
    holidayServiceImpl.getResponseStatusAccordingToCurrentDate(Mockito.any());
  }

  @Test
  public void validateIfCurrentHourOutOfRange() {
    when(holidayServiceImpl.isCurrentHourOutOfRange()).thenReturn(null);
    LocalTime startHourTest = LocalTime.of(7, 00, 00);
    assertTrue(startHourTest.isBefore(LocalTime.of(8, 00, 00)));
    LocalTime endHourTest = LocalTime.of(22, 01, 00);
    assertTrue(endHourTest.isAfter(LocalTime.of(22, 00, 00)));
    assertTrue(startHourTest.isBefore(LocalTime.of(8, 00, 00))
        || endHourTest.isAfter(LocalTime.of(22, 00, 00)));
  }

  @Test
  public void getResponseStatusAccordingToCurrentDateTest() {
    HolidayResponse expectedResponse = new HolidayResponse();
    expectedResponse.setToday("2020-02-26");
    List<HolidayModel> holidays = new ArrayList<>();
    holidays.add(new HolidayModel("2020-01-01"));
    expectedResponse.setHolidays(holidays);
    Mockito.when(holidaysFeignClient.getHolidays(getHeaders()))
        .thenReturn(Optional.of(expectedResponse));
    holidayServiceImpl.getResponseStatusAccordingToCurrentDate(getHeaders());

    assertNotNull(expectedResponse);
  }

}
