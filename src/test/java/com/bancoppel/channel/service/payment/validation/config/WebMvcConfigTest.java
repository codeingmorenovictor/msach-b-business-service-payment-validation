/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bancoppel.channel.service.payment.validation.config;

import static org.junit.Assert.assertNotNull;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import com.bancoppel.channel.service.payment.validation.constant.SpecialCharacterConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

/**
 * Class that defines the methods to test the WebMvcConfig class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
public class WebMvcConfigTest {

  /**
    * Component that will be tested.
    */
  @InjectMocks
  private WebMvcConfig webMvcConfig;
	    
  /**
    * Mock component to get information of a file properties.
    */
  @MockBean
  private ApiConstants apiConstants;

  /**
   * Initialization of variables to simulate information to be tested.
   */
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test for webConfig to ignore accept header.
   */
  @Test
  public void test() {
     ContentNegotiationConfigurer configurer = new ContentNegotiationConfigurer(null) ;
     webMvcConfig.configureContentNegotiation(configurer);
     assertNotNull(configurer);
  }
  
  /**
   * Test for webConfig to ignore accept header.
   * 
   * Given ContentNegotiationConfigurer and WebMvcConfig,
   * When the method configureContentNegotiation is invoked,
   * Then add to the configuration of the API the ContentNegotiationConfigurer.
   */
  @Test
  public void testResourceHandlers() {

    Mockito.when(apiConstants.getLabel()).thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    Mockito.when(apiConstants.getResourceLocation()).thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    Mockito.when(apiConstants.getWebjars()).thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    Mockito.when(apiConstants.getWebjarsLocation()).thenReturn(SpecialCharacterConstants.EMPTY_STRING);
    
    ResourceHandlerRegistry registry = Mockito.mock(ResourceHandlerRegistry.class);     
    ResourceHandlerRegistration resourceHandlerRegistration = Mockito.mock(ResourceHandlerRegistration.class);
    Mockito.when(registry.addResourceHandler(Mockito.anyString())).thenReturn(resourceHandlerRegistration);
    webMvcConfig.addResourceHandlers(registry);
    assertNotNull(registry);   
  }

}
