/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bancoppel.channel.service.payment.validation.component;

import static org.junit.Assert.assertNotNull;
import com.bancoppel.channel.service.payment.validation.exceptions.custom.DownstreamException;
import com.bancoppel.channel.service.payment.validation.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import feign.FeignException;
import feign.Request;
import feign.Request.HttpMethod;
import feign.Response;

/**
 * Class that defines the methods to test the CustomFeignErrorDecoder class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class CustomFeignErrorDecoderTest {
  
  /**
   * Static variable to record.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(CustomFeignErrorDecoderTest.class);
  
  /** Injected component that creates an instance of CustomFeignErrorDecoder with the mock beans. */
  @InjectMocks
  private CustomFeignErrorDecoder customFeignErrorDecoder;
  
  /** Creating a ObjectMapper object as a MockBean object. */
  @MockBean
  private ObjectMapper mockedMapper;
  
  /**
   * This method fills all the fields necessary to simulate the operations of the SchedulePaymentBusiness.
   */
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }
  
  /**
   * 
   * Test simulated test throws an exception of the FeignException type.
   * 
   * Given the response of a Api that is requested by "feign" (the response as a String),
   * Then return  Micro service name.
   */
  @Test
  public void decode() {

    String reason = "HTTP/1.1 401 \n" + 
        "content-type: application/json;charset=UTF-8\n" + 
        "date: Wed, 05 Sep 2018 16:40:33 GMT\n" + 
        "transfer-encoding: chunked\n" + 
        "x-application-context: mafch-d-bne-account-multibalance:7801\n" + 
        "\n" + 
        "feign.okhttp.OkHttpClient$1@3b9d3aa8";
    
    String body =
        "{\"type\":\"ERROR\",\"code\":\"BNEE-401\",\"details\":\"Not Authorized\",\"location\":\"\\/api\\/private\\/v1\\/channels\\/bne\\/accounts\\/balance\",\"moreInfo\":\"\",\"uuid\":\"uuid1\",\"timestamp\":\"2018-09-17T19:34:03-0500\"}";

    String methodKey = "POST";
    Map<String,Collection<String>> map = new HashMap<String, Collection<String>>();
    
    Request request = Request.create(HttpMethod.POST, "http://test/", map, null);

    
    Response response = Response.builder().reason(reason).status(400).headers(map).body(body, Charset.defaultCharset()).request(request).build();
    DownstreamException downstreamException = (DownstreamException) customFeignErrorDecoder.decode(methodKey, response);
    LOGGER.info("json -> " + Util.getJson(downstreamException.getMessage()));
    assertNotNull(downstreamException);
  }
  
  
  /**
   * 
   * Test simulated test throws an exception of the FeignException type, caused by a body null.
   * 
   * Given the response of a Api that is requested by "feign" (the response as a String) and
   * the body response is null,
   * Then return  FeignException.
   */
  @Test
  public void decodeBodyNull() {

    String reason = "HTTP/1.1 401 \n" + 
        "content-type: application/json;charset=UTF-8\n" + 
        "date: Wed, 05 Sep 2018 16:40:33 GMT\n" + 
        "transfer-encoding: chunked\n" + 
        "x-application-context: mafch-d-bne-account-multibalance:7801\n" + 
        "\n" + 
        "feign.okhttp.OkHttpClient$1@3b9d3aa8";
    
    String methodKey = "POST";
    Map<String,Collection<String>> map = new HashMap<String, Collection<String>>();
    
    Request request = Request.create(HttpMethod.POST, "http://test/", map, null);

    
    Response response = Response.builder().reason(reason).status(400).headers(map).body(null, Charset.defaultCharset()).request(request).build();
    FeignException downstreamException = (FeignException) customFeignErrorDecoder.decode(methodKey, response);
    LOGGER.info("json -> " + Util.getJson(downstreamException.getMessage()));
    assertNotNull(downstreamException);
  }

  /**
   * 
   * Test simulated test throws an exception of the FeignException type, caused by a body wrong.
   * 
   * Given the response of a Api that is requested by "feign" (the response as a String) and the 
   * body response is wrong,
   * Then return FeignException.
   */
  @Test
  public void decodeBodyWrong() {

    String reason = "HTTP/1.1 401 \n" + 
        "content-type: application/json;charset=UTF-8\n" + 
        "date: Wed, 05 Sep 2018 16:40:33 GMT\n" + 
        "transfer-encoding: chunked\n" + 
        "x-application-context: mafch-d-bne-account-multibalance:7801\n" + 
        "\n" + 
        "feign.okhttp.OkHttpClient$1@3b9d3aa8";
    
    String body = "";
    
    
    String methodKey = "POST";
    Map<String,Collection<String>> map = new HashMap<String, Collection<String>>();
    
    Request request = Request.create(HttpMethod.POST, "http://test/", map, null);

    
    Response response = Response.builder().reason(reason).status(400).headers(map).body(body, Charset.defaultCharset()).request(request).build();
    
    FeignException downstreamException = (FeignException) customFeignErrorDecoder.decode(methodKey, response);
    LOGGER.info("json -> " + Util.getJson(downstreamException.getMessage()));
    assertNotNull(downstreamException);
  }
  
  
  
}
