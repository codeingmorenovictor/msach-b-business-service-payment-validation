/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bancoppel.channel.service.payment.validation.config;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Class that defines the methods to test the ModelMapperConfig class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ModelMapperConfigTest {

  /**
   * Component that will be tested.
   */  	
  @InjectMocks
  private ModelMapperConfig modelMapperConfig;

  /**
   * Test to simulate the model mapper creation.
   * 
   * Given a ModelMapper Bean.
   * When the application is being configured.
   * Then the bean ModelMapper is added to the application context.
   */
  @Test
  public void modelMapperTest() {
	  assertNotNull(modelMapperConfig.modelMapper());
  }
  
}
