/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bancoppel.channel.service.payment.validation.component;

import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Class that defines the methods to test the TransactionLogger class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class TransactionLoggerTest {

  /**
   * Component that will be tested.	
   */
  @InjectMocks
  private TransactionLogger transactionLogger;

  /**
   * Constant that is used to simulate an error message.
   */
  private static final String TRANSACTION_ERROR = "ERROR";
  /**
   * Constant that is used to simulate the name of transaction.
   */
  private static final String TRANSACTION = "transaction";
  /**
   * Constant that is used to simulate an message.
   */
  private static final String MESSAGE = "message";
  /**
   * Constant that is used to simulate an error code.
   */
  private static final String ERROR_CODE = "errorCode";
  /**
   * Constant that is used to simulate an error description.
   */
  private static final String ERROR_DESCRIPTION = "errorDescription";
  /**
   * Constant that is used to simulate an error code empty.
   */
  private static final String ERROR_CODE_EMPTY = "";

  /**
   * Initialization of variables to simulate information to be tested.
   */
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * This test is to simulate the operation that sets user and error data.
   * 
   * Given a ERROR_CODE and ERROR_DESCRIPTION requests,
   * When setErrorData is invoked,
   * Then set ERROR_CODE and ERROR_DESCRIPTION to the TransactionLogger
   * and generate the String of log.
   */
  @Test
  public void transactionLoggerSetUserDataSetErrorDataAndToStringTest() {
    transactionLogger.setErrorData(ERROR_CODE, ERROR_DESCRIPTION);
    transactionLogger.toString();
    assertNotNull(transactionLogger.toString());
  }

  /**
   * This test is to simulate the operation that sets user data, error data and to String.
   * 
   * Given a ERROR_CODE_EMPTY and ERROR_DESCRIPTION requests,
   * When setErrorData is invoked,
   * Then set ERROR_CODE_EMPTY and ERROR_DESCRIPTION to the TransactionLogger
   * and generate the String of log.
   */
  @Test
  public void transactionLoggerSetUserDataSetErrorDataAndToStringErrorCodeEmptyTest() {
    transactionLogger.setErrorData(ERROR_CODE_EMPTY, ERROR_DESCRIPTION);
    transactionLogger.toString();
    assertNotNull(transactionLogger.toString());
  }

  /**
   * This test is to simulate the operation that start and stop the transaction operation.
   * 
   * Given a TRANSACTION_ERROR requests,
   * When a transaction fail,
   * Then the transaction is started with a TRANSACTION_ERROR, the transaction is stopped 
   * and the registration chain is generated.
   */
  @Test
  public void startTransactionFailureTest() {
    transactionLogger.startTransaction(TRANSACTION_ERROR);
    transactionLogger.stopTransaction();
    transactionLogger.toString();
    assertNotNull(transactionLogger.toString());
  }

  /**
   * This test is to simulate the operation that start transaction operation.
   * 
   * Given a TRANSACTION_ERROR requests,
   * When a transaction starts,
   * Then the transaction is started with a TRANSACTION.
   */
  @Test
  public void transactionLoggerStartTransactionTest() {
    transactionLogger.startTransaction(TRANSACTION);
    assertNotNull(transactionLogger.toString());
  }

  /**
   * This test is to simulate the operation that start transaction operation.
   * 
   * Given a TRANSACTION_ERROR requests,
   * When a transaction starts,
   * Then the transaction is started with a TRANSACTION.
   */
  @Test
  public void transactionLoggerMessageTest() {
    transactionLogger.message(TRANSACTION, MESSAGE);
    assertNotNull(transactionLogger.message(TRANSACTION, MESSAGE));
  }

}
