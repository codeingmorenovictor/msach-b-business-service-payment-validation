/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bancoppel.channel.service.payment.validation.config;

import static org.junit.Assert.assertNotNull;
import com.bancoppel.channel.service.payment.validation.constant.ApiConstants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Class that defines the methods to test the SwaggerConfig class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class SwaggerConfigTest {

  /**
   * Component that will be tested.
   */
  @InjectMocks
  private SwaggerConfig swaggerConfig;

  /**
   * Bean required by the SwaggerConfig component. 
   */
  @Mock
  private Docket docket;
  
  /**
   * Component to get messages from properties file.
   */
  @Mock
  private ApiConstants apiConstants;


  /**
   * Test to simulate the productApi flow.
   * 
   * Given a Docket Bean,
   * When the productApi is invoket,
   * Then a new Docket with the information of the API is added to configution.
   * 
   */
  @Test
  public void productApiTest() {
	  assertNotNull(swaggerConfig.productApi());
  }
}
